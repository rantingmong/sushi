//: Playground - noun: a place where people can play

import Cocoa

var memory                                      = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ]

var freeBlocks:[ (location: Int, size: Int) ]   = [ (location: 2, size: 1), (location: 5, size: 2), (location: 8, size: 1) ]

print(memory)

// mark all free blocks into memory

var freeLoc     = freeBlocks[0].location
var freeSize    = freeBlocks[0].size - 1

freeBlocks.remove(at: 0)

for index in 0 ..< memory.count {

    if freeLoc == index {

        memory[index] = 0

        if freeBlocks.count > 0 {

            if freeSize == 0 {

                // move to next free block

                freeLoc     = freeBlocks[0].location
                freeSize    = freeBlocks[0].size - 1

                freeBlocks.remove(at: 0)
            }
            else {

                freeSize -= 1
                freeLoc  += 1
            }
        }
    }
}

print(memory)

// compact the memory

var lastCompactionIndex = -1

for index in 0 ..< memory.count {

    print("index \(index), lci \(lastCompactionIndex)", separator: "", terminator: ": ")

    if      memory[index] == 0 && lastCompactionIndex <= -1 {

        lastCompactionIndex = index
    }
    else if memory[index] != 0 && lastCompactionIndex > 0 && index > lastCompactionIndex {

        memory[lastCompactionIndex] = memory[index]

        memory[index]               = 0
        lastCompactionIndex        += 1
    }

    print(memory)
}

